# Āti

Sort of a random project I started

![NPM](https://img.shields.io/npm/l/ati)

![Visual Studio Code](https://badges.aleen42.com/src/visual_studio_code.svg)

![ESLint](https://badges.aleen42.com/src/eslint.svg)

![JavaScript](https://badges.aleen42.com/src/javascript.svg)

![GitLab](https://badges.aleen42.com/src/gitlab.svg)

## Usage

Install first!
`npm i ati.js`

Discord Snowflake
```javascript
const ati = require('ati.js'); // Require the package

console.log(ati.discordSnowflake()); // Will return a sort of discord snowflake
```

Twitter Snowflake
```javascript
const ati = require('ati.js'); // Require the package

console.log(ati.twitterSnowflake()); // Will return a sort of twitter snowflake
```

Custom Snowflake
```javascript
const ati = require('ati.js'); // Require the package

console.log(ati.snowflake('epoch here')) // Epoch example: 2000-01-01T00:00:00.000Z
```

# Deconstruction

Discord Snowflake
```javascript
const ati = require('ati.js'); // Require the package

console.log(ati.discordSnowflake.deconstruct("Your snowflake here")); // Will deconstruct a discord type snowflake.
```
or

```javascript
const ati = require('ati.js'); // Require the package

const snowflake = ati.discordSnowflake(); // Generation

console.log(ati.discordSnowflake.deconstruct(snowflake)); // Will deconstruct a discord snowflake.
```

Twitter Snowflake
```javascript
const ati = require('ati.js'); // Require the package

const snowflake = ati.twitterSnowflake(); // Generation

console.log(ati.twitterSnowflake.deconstruct(snowflake)); // Deconstruction
```

# Docs

[Docs Here](https://raven-studio.gitlab.io/random/ati/docs/)

For any issues or questions feel free to open up an issue on the [GitLab Repo](https://gitlab.com/raven-studio/random/ati)


# Notice

Some functions may not be documented. Reasons:

- It could be that I'm unsure about something
- It still needs work
- I don't really want to document it