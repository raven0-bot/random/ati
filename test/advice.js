const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('advice', function() {
	it('should return a String', async function() {
		const data = await ati.advice();

		assert.typeOf(data, 'string');
	}).timeout(5000);
});