const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('discordSnowflakeTest', function() {
	it('should return a BigInt', function() {
		const snowflake = ati.discordSnowflake();

		assert.typeOf(snowflake, 'bigint');
	});
});