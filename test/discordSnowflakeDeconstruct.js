const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('discordSnowflakeTest', function() {
	it('should return a Object', function() {
		const snowflake = BigInt(823486283757260800);
		const flake = ati.discordSnowflake.deconstruct(snowflake);

		assert.typeOf(flake, 'object');
	});
});