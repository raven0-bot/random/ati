const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('fact', function() {
	it('should return String', async function() {
		const data = await ati.fact();

		assert.typeOf(data, 'string');
	}).timeout(5000);
});