const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('furry', function() {
	it('should return a Object', async function() {
		const data = await ati.furry('animals', 'birb');

		assert.typeOf(data, 'Array');
	});
});