const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('ParseURL', function() {
	it('should return a URL Object', function() {
		const url = 'https://www.npmjs.com/package/@raven-studio/blu';
		const parsed = ati.parseURL(url);

		// Add  .toString() for the original UR:
		assert.typeOf(parsed, 'URL');
	});
});