const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('snowflake', function() {
	it('should return a BigInt', async function() {
		const snowflake = ati.snowflake('2000-01-01T00:00:00.000Z');

		assert.typeOf(snowflake, 'bigint');
	});
});