const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('TwitterSnowflake', function() {
	it('should return a BigInt', function() {
		const snowflake = ati.twitterSnowflake();

		assert.typeOf(snowflake, 'bigint');
	});
});