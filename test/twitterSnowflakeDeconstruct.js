const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('twitterSnowflakeDeconstruct', function() {
	it('should return a Object', function() {
		const snowflake = 1985896618192408576;
		const flake = ati.twitterSnowflake.deconstruct(snowflake);

		assert.typeOf(flake, 'object');
	});
});