const assert = require('chai').assert;
const ati = require('../ati.js');

/* eslint-disable no-undef */
describe('Uwuify', function() {
	it('should return H-Hello!!11 It\'s nice to meet you', function() {
		assert.equal(ati.uwuify('Hello! It\'s nice to meet you'), 'H-Hello!!11 It\'s nice t-to meet you');
	});
});